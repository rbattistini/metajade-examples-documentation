= MetaJade Examples Documentation Site

This is the playbook project for the documentation site of the MetaJade Examples.
The site is available at https://metajade.gitlab.io/metajade-examples-documentation/bank-account-example-metajade/1.0.0/index.html.
